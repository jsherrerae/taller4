#El programa imprime la suma, la resta, la multiplicación de tres números(x,y,z) y sus identidades trigonometricas:
import math

x=input("ingrese x: ")
y=input("ingrese y: ")
z=input("ingrese z: ")

x=float(x)
y=float(y)
z=float(z)

suma=x+y+z
resta=x-y-z
multiplicación=x*y*z
seno=math.radians(x)
coseno=math.radians(y)
tangente=math.radians(z)

print(suma)
print(resta)
print(multiplicación)
print(math.sin(seno))
print(math.cos(coseno))
print(math.tan(tangente))